Launch
======

The qdo python/command\-line interfaces allow to create workers to execute tasks
within batch jobs. A worker is equivalent to an autonomous execution of qdo do,
that processes tasks from a queue. There are two types of workers:

* Non MPI workers: Execute tasks that do not require MPI Processing
  Elements (PE). These workers can process tasks that require at most as 
  many cores as the ones present in a compute node or NUMA node.
 
* MPI workers: Execute tasks that require MPI PEs (mpiproc\_per\_worker). 
  Each PE can allocate one or more cores (cores\_per\_mpiproc). We assumed that MPI
  Workers can not share nodes. In case Open MP and this variable is not set by
  the user, QDO will set the environment variable OMP_NUM_THREADS to the
  resulting number of cores per MPI PE in the worker.
  
Workers can run (and execute tasks) in parallel within a 
*single job, or divided in multiple jobs* 
to reduce the per-job wait time (as their number of
cores requirement is smaller).

Finally workers can be placed using *NUMA packing*: For non MPI workers,
this means that a single worker will not allocate cores from different
NUMAs within a node. For MPI workers, this means that a single worker PE
will not allocate cores from different NUMAs.

# Workers definition parameters #


Worker parameters are set according to  the task requirements and the resource
characteristics (cores per node and cores per NUMA). QDO will do the
required calculations to set the parameters to run the corresponding
aprun/mpirun required to launch workers. If mpiproc\_per\_worker is 0, workers will
not use any PEs and considered non-MPI. If mpiproc\_per\_worker > 0, workers will be
MPI workers.

## Non MPI workers for non MPI tasks ##

* nworkers: Number of workers to launch, if nworkers workers are created,
nworkers tasks are processed in parallel.

* mpiproc\_per\_worker. Set to 0, since processed tasks do not require MPI.
 
* cores\_per\_worker: cores required by each worker.

* numa\_pack: activate/deactivate NUMA packing. 
 
* njobs: Number of jobs among which the nworkers will be spread.

Non MPI workers in the same job are launched as an MPI job in which each
worker is an MPI PE.

e.g. launching 4 workers with 10 cores per task, numa\_packing in one job.
QDO would allocate 2 nodes, and run 2 workers in each node.

    q.launch(4, cores\_per\_worker=10, numa\_pack=False) 
    qdo launch queue\_name 4 --cores\_per\_worker=10

### Non MPI Summary ###

Cores per Node  = 24

Cores per Numa  = 12

mpiproc\_per\_worker = 0 

cores\_per\_mpiproc = None

| nworkers  | numa\_pack | cores\_per\_worker | total\_cores  | pes\_per\_node | pes\_per\_numa |
| --------- | ------- | -------------- | ------------ | ------------ | ------------ |
| 24        | F       | 1              | -> 24        | -> 24        | -> N/A       |
| 36        | F       | 1              | -> 48        | -> 24        | -> N/A       |
| 24        | F       | 2              | -> 48        | -> 12        | -> N/A       |
| 10        | F       | 3              | -> 48        | -> 8         | -> N/A       |
| 20        | T       | 5              | -> 120       | -> 4         | -> 2         |
| 20        | F       | 7              | -> 168       | -> 3         | -> N/A       |
| 20        | T       | 7              | -> 240       | -> 2         | -> 1         |
| 20        | F       | 25             | -> Excep1    | -> Excep1    | -> Excep1    |
| 20        | T       | 13             | -> Excep2    | -> Excep2    | -> Excep2    |


Excep1: One Non MPI workers cannot allocate cores across nodes.

Excep2: One Non MPI workers cannot allocate cores across NUMAs when Numa pack
is activated.

## MPI workers ##

* nworkers: Number of workers to launch.

* mpiproc\_per\_worker > 0: Number of PEs present in each worker.  
 
* cores\_per\_mpiproc: cores per MPI PE within a worker.

* cores\_per\_worker: cores required by each worker.

* numa\_pack: activate/deactivate NUMA packing.
 
* njobs: Number of jobs among which the nworkers will be spread.

There are three different strategies to define the MPI workers. In all of
them: mpiproc\_per\_worker and at least one of cores\_per\_worker or cores\_per\_mpiproc
have to be defined (and of course if NUMA packing is required).

### MPI Summary ###

For one MPI Worker

Cores per Node  = 24

Cores per Numa  = 12

| mpiproc\_per\_worker | numa\_pack | cores\_per\_mpiproc | cores\_per\_worker | pes\_per\_node | pes\_per\_numa |
| --------- | ------- | ------------ | -------------- | ------------ | ------------ |
| (Estr1)   |         |              |                |              |              |
| 1         | F       | 1            | -> 24          | -> 1         | -> N/A       |
| 1         | T       | 1            | -> 24          | -> 1         | -> 1         |
| 12        | F       | 1            | -> 24          | -> 12        | -> N/A       |
| 12        | T       | 1            | -> 24          | -> 12        | -> 6         |
| 12        | F       | 4            | -> 48          | -> 12        | -> N/A       |
| 12        | T       | 4            | -> 48          | -> 6         | -> 3         |
| 12        | F       | 7            | -> 96          | -> 3         | -> N/A       |
| 12        | F       | 7            | -> 148         | -> 2         | -> 1         |
| (Estr2)   |         |              |                |              |              |
| 1         | F       | -> 24        | 24             | -> 1         | -> N/A       |
| 1         | T       | -> 12        | 24             | -> 1         | -> 1         |
| 3         | F       | -> 8         | 24             | -> 3         | -> N/A       |
| 7         | T       | -> 3         | 24             | -> 7         | -> 4         |
| 3         | T       | -> 6         | 24             | -> 3         | -> 2         |
| 1         | F       | -> Excep1    | 48             | -> Excep1    | -> N/A       |
| 1         | T       | -> Excep2    | 24             | -> Excep2    | -> Excep2    |
| (Estr3)   |         |              |                |              |              |
| 3         | F       | 3            | 24             | -> 3         | -> N/A       |
| 3         | T       | 3            | 24             | -> 3         | -> 2         |
| 3         | T       | 3            | 48             | -> 2         | -> 1         |
| 8         | T       | 4            | 24             | -> Excep3    | -> Excep3    |


Excep1: One workers cannot have a PE using more core than present in a node.
This would imply allocating cores across nodes.

Excep2: One workers cannot have a PE using more core than present in a NUMA.
This would imply allocating cores across NUMAs (if Numa pack activated).

Excep3: cores\_per\_worker not enough for the requested cores\_per\_mpiproc and mpiproc\_per\_worker.

###Strategy one, Minimum number of cores###

Define the number of PEs, cores required by PE, NUMA packing and, let QDO 
determine how many cores each worker require. Used by setting mpiproc\_per\_worker,
 cores\_per\_mpiproc, numa\_pack, and setting cores\_per\_worker to NONE. 

e.g. one worker with 4 PEs, 12 cores per PE, no NUMA Packing, in a system 
with 24 cores per node. QDO would launch a job for 2 nodes (12*4 cores=
48 cores = 2 nodes), 2 PEs per Node. 

    q.launch(1, mpiproc\_per\_worker=4, cores\_per\_mpiproc=12, numa\_pack=False)
    qdo launch queue\_name 1 --mpiproc\_per\_worker=4 --cores\_per\_mpiproc=12

e.g. one worker with 3 PEs, 8 cores per PE, NUMA Packing, in a system 
with 24 cores per node and 12 cores per NUMA. In this case, QDO cannot place
more than one PE per NUMA (two PEs would sum 16 cores, more than a NUMA, 12).
As a consequence it requires 3 NUMAs, two nodes, 48 cores to run this worker 
(With no NUMA packing it would fit in one single node: 3*8 = 24 cores).

    q.launch(1, mpiproc\_per\_worker=3, cores\_per\_mpiproc=8, numa\_pack=True)
    qdo launch queue\_name 1 --mpiproc\_per\_worker=3 --cores\_per\_mpiproc=8 --numapack
 
###Strategy two, cores per PE are non important, only memory###

Define the number of PEs, and the number of cores (in the end nodes) required by
the worker. QDO will spread the PEs equally along the nodes calculating the
maximum number of cores that each PE may have. Used by setting mpiproc\_per\_worker,
 cores\_per\_worker, numa\_pack, and setting cores\_per\_mpiproc to NONE. 

e.g. one worker with 4 PEs, 24 cores per worker, no NUMA Packing, in a 
system with 24 cores per node. QDO would launch a job for 1 nodes, 4
PEs per Node, 6 cores each PE.

    q.launch(1, mpiproc\_per\_worker=4, cores\_per\_worker=24, numa\_pack=False)
    qdo launch queue\_name 1 --mpiproc\_per\_worker=4 --taskcores=24  

e.g. one worker with 10 PEs, 48 cores per worker, NUMA Packing, in a 
system with 24 cores per node and 12 cores per NUMA. QDO would divide the
PEs among the 4 NUMAs rounding up to 3 PEs per NUMA and this, 4 cores per
PE. 

    q.launch(1, mpiproc\_per\_worker=10, cores\_per\_worker=48, numa\_pack=True)
    qdo launch queue\_name 1 --mpiproc\_per\_worker=10 --taskcores=48 --numapack 

###Strategy three, Full control###

Number of cores per PE is relevant but we may want to assign more cores (in the end nodes) per worker to increase the memory per PE. Used by setting mpiproc\_per\_worker, cores\_per\_mpiproc, cores\_per\_worker, and numa\_pack.

e.g. one worker with 4 PEs, 2 cores per PE, but 48 cores per worker, no NUMA Packing. QDO would allocate 2 nodes, running 2 PEs in each node, increase
the memory per PE (compare to not setting the cores\_per\_worker and running
all the PEs in the same node).

q.launch(1, mpiproc\_per\_worker=4, cores\_per\_mpiproc=2, cores\_per\_worker=48, numa\_pack=False)
qdo launch queue\_name 1 --mpiproc\_per\_worker=4 --cores\_per\_mpiproc=2 --cores\_per\_worker=48
 
# Other examples #

Running 10 non-MPI workers in 2 jobs (5 each):

    q.launch(10, njobs=2, cores\_per\_worker=12)
    qdo launch queue\_name 10, --jobs=2 --cores\_per\_worker=12

# Known Restrictions #

* nworkers has to be divisible by njobs.

* cores\_per\_worker has to be divisible by cores\_per\_node

* non MPI workers cannot allocate (taskcores) more cores than a single node.

* non MPI workers with NUMA packing cannot allocate more cores than a single
  NUMA.
 
*  At least cores\_per\_mpiproc or cores\_per\_worker must be set.

* PEs in MPI workers cannot allocate (cores\_per\_mpiproc) more cores than a single node.

* PEs in MPI workers with NUMA packing cannot allocate more cores than a
  single NUMA.
  
* In Strategy 3: cores\_per\_worker has to be equal or larger than the number
  or cores required to map mpiproc\_per\_worker with cores\_per\_mpiproc.

# Supported systems #

Interface with submission systems is defined in the within package file
batch\_config.ini. Currently supported systems are Hopper, Edison, and Slurm-Babbage.





