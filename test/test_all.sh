#!/bin/bash
# Script to launch all the qdo tests setting appropiate enviroment variables and
# storing result in a file named ${backend_name}.${test_name}.test_result.
#
# If invoked without arguments it will test the sqlite and postgres backends.
# otherwise use as:
# ./test_all.sh backend_name
#
# Requirements:
# - General: Functional python environment that can use qdo.
# - sqlite: Environment configured to use QDO normaly.
# - postgres: 
#	- A running Postgres instance
#	- QDO_DB_USER amd QDO_DB_PASS configured for a user with admin permissions
#     on the postgres db.


test_list_sqlite=("test_console" "test_launch_api" "test_launch_call" \
		 "test_pyapi_log" "test_qdorc.TestQDORC")

test_list_postgres=${test_list_sqlite[@]}
test_list_postgres+=("test_postgres.TestAdminModule" \
	"test_postgres.TestPostgres2" "test_postgres.PostgresStressTestCase")

passed_count=0
failed_count=0

if [ $# -ge 1 ]; then
	backend_list=($1)
else
	backend_list=("sqlite" "postgres")
fi

for backend_name in ${backend_list[@]}; do
	export QDO_BACKEND=${backend_name}
	test_list=test_list_${QDO_BACKEND}
	
	# Setting up the postgress backend
	if [ $backend_name == "postgres" ]; then
		if [ -n "${QDO_DB_USER+1}" ] ; then
			old_QDO_DB_USER=$QDO_DB_USER
		fi
		if [ -n "${QDO_DB_PASS+1}" ]; then
			old_QDO_DB_PASS=QDO_DB_PASS
		fi
		postgres_QDO_DB_USER="finalunittestuser"
		postgres_QDO_DB_PASS="finalunittestpass"
		postgres_QDO_DB_NAME="finalunittestdb"		
		../bin/qdo-pgadmin createdb $postgres_QDO_DB_NAME
		
		export QDO_DB_NAME=$postgres_QDO_DB_NAME
		export QDO_DB_HOST="127.0.0.1"
		../bin/qdo-pgadmin --db $postgres_QDO_DB_NAME delprj $postgres_QDO_DB_USER 
		../bin/qdo-pgadmin --db $postgres_QDO_DB_NAME deluser $postgres_QDO_DB_USER 
		../bin/qdo-pgadmin --db $postgres_QDO_DB_NAME createuser $postgres_QDO_DB_USER $postgres_QDO_DB_PASS
		
	fi
	eval "test_list=\${$test_list[@]}"
	echo ".......Testing backend ${QDO_BACKEND}: ${test_list[@]}}"
	for test_name in ${test_list[@]}; do
		if [ $backend_name == "postgres" ]; then
			if [ $test_name == "test_postgres.TestAdminModule" ] || \
				[ $test_name == "test_pyapi_log" ] ; then
				# Configuration for tests that require the default user which
				# has admin permisions on the DB.
				unset QDO_DB_USER
				unset QDO_DB_PASS
				unset QDO_DB_NAME
				if [ -z "$old_QDO_DB_USER" ]; then
					export QDO_DB_USER=$old_QDO_DB_USER	
				fi
				if [ -z "$old_QDO_DB_PASS" ]; then
					export QDO_DB_PASS=$old_QDO_DB_PASS	
				fi 

			else
				# Configuration for tests that can use the created DB.
				export QDO_DB_USER=$postgres_QDO_DB_USER	
				export QDO_DB_PASS=$postgres_QDO_DB_PASS
				export QDO_DB_NAME="finalunittestdb"

			fi
		fi
		test_output_file="${QDO_BACKEND}.${test_name}.test_result"
		command="python -m unittest ${test_name}"
		printf "Test ${test_output_file}: "
		result="FAILED. Re-run: $command"
		$command &> "${test_output_file}"
		if [ $? -eq 0 ]; then
			result="PASSED"
			passed_count=$[ ${passed_count}+1 ]
		else
			failed_count=$[ ${failed_count}+1 ]
		fi
		echo "${result}"
	done
done
# Some clean up: remove created user
# TODO(gonzalorodrigo): Remove created database
unset QDO_DB_USER
unset QDO_DB_PASS
unset QDO_DB_NAME
if [ -z "$old_QDO_DB_USER" ]; then
	export QDO_DB_USER=$old_QDO_DB_USER	
fi
if [ -z "$old_QDO_DB_PASS" ]; then
	export QDO_DB_PASS=$old_QDO_DB_PASS	
				fi 
../bin/qdo-pgadmin --db $postgres_QDO_DB_NAME delprj $postgres_QDO_DB_USER 
../bin/qdo-pgadmin --db $postgres_QDO_DB_NAME deluser $postgres_QDO_DB_USER 
		


total_tests=$[ ${passed_count}+${failed_count} ]
echo ".......Summary......."
echo "Total tests: ${total_tests}."
echo "Tests PASSED: ${passed_count}"
echo "Tests FAILED: ${failed_count}"
if [ $failed_count -eq 0 ]; then
	echo "ALL TESTS PASSED!! OLÉ!!!"
	exit 0
else
	echo "SOME TESTS FAILED!! Keep trying..."
	exit 1
fi
