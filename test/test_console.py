#!/usr/bin/env python

"""
Test qdo command line interface.
"""

from __future__ import print_function

import sys
import os
import unittest

import qdo
from qdo import console

NTASKS = 3

def _test_ntasks(q, **kwargs):
    """
    Check the number of tasks in each state.
    If a state isn't listed in kwargs, assume 0.
    """
    ntasks = q.tasks(summary=True)
    for state in ['Waiting', 'Pending', 'Running', 'Succeeded', 'Failed']:
        n = kwargs[state] if (state in kwargs) else 0            
        failmsg = "{}={} instead of {}".format(state, ntasks[state], n)
        assert ntasks[state] == n, failmsg
    

class CommandLineTestCase(unittest.TestCase):
    def setUp(self):  # also tests qdo create
        self.qname = 'TestBlatFoo'
        self.cmdfile = self.qname + '.txt'
        assert 0 == console.qdo(['create', self.qname])
        self.q = qdo.connect(self.qname)

    def tearDown(self):
        self.q.delete()
        if os.path.exists(self.cmdfile):
            os.remove(self.cmdfile)

    def test_delete(self):
        assert 256 == console.qdo(['delete', self.qname])
        assert 0 == console.qdo(['delete', self.qname, '--force'])
        # connecting to the deleted queue should error:
        self.assertRaises(Exception, qdo.connect, self.qname)

    def test_remaining_commands(self):
        """Test several qdo commands.

        These are lumped into a single method because they use the changing
        state of the queue from one command to the next.
        """

        with open(self.cmdfile, 'w') as fx:
            for i in range(NTASKS):
                fx.write('echo Goodbye {}\n'.format(i))

        for i in range(NTASKS):
            console.qdo(['add', self.qname, "echo Hello {}".format(i)])

        # All tasks should now be pending
        _test_ntasks(self.q, Pending=NTASKS)

        #----- qdo load
        assert 0 == console.qdo(['load', self.qname, self.cmdfile])
        assert 0 == console.qdo(['load', self.qname, self.cmdfile,
                                 '--priority', '1'])
        # TODO: not sure how to test this:
        #assert 0 == os.system('cat {} | qdo load {} -'
        #                      .format(self.cmdfile, self.qname))
        # so I'm just doing this again:
        assert 0 == console.qdo(['load', self.qname, self.cmdfile])
        _test_ntasks(self.q, Pending=4*NTASKS)

        #----- Load some tasks that should fail
        for i in range(NTASKS):
            console.qdo(['add', self.qname, 'exit {:d}'.format(i+1)])
        _test_ntasks(self.q, Pending=5*NTASKS)

        #----- qdo status
        assert 0 == console.qdo(['status', self.qname])

        #----- qdo tasks [--state STATE] [--verbose]
        assert 0 == console.qdo(['tasks', self.qname])
        assert 0 == console.qdo(['tasks', self.qname, '--state', 'Pending'])
        assert 0 == console.qdo(['tasks', self.qname, '--verbose'])

        #----- qdo do
        assert 0 == console.qdo(['do', self.qname, '--timeout', '1'])
        _test_ntasks(self.q, Succeeded=4*NTASKS, Failed=NTASKS)

        #----- qdo retry
        assert 0 == console.qdo(['retry', self.qname])
        _test_ntasks(self.q, Succeeded=4*NTASKS, Pending=NTASKS)

        # Not implemented in postgres
        if (not "QDO_BACKEND" in os.environ.keys() or
            os.environ["QDO_BACKEND"]!="postgres"):
        #----- qdo retry with exit code filter
            assert 0 == console.qdo(['do', self.qname, '--timeout', '1'])
            assert 0 == console.qdo(['tasks', self.qname, '--exitcode', '1'])
            _test_ntasks(self.q, Succeeded=4*NTASKS, Failed=NTASKS)
            for i in range(1,NTASKS+1):
                assert 0 == console.qdo(['retry', self.qname, '--force',
                                         '--exitcode', str(i)])
                _test_ntasks(self.q, Succeeded=4*NTASKS, Pending=i,
                             Failed=NTASKS-i)

        #----- qdo rerun
        assert 256 == console.qdo(['rerun', self.qname])
        _test_ntasks(self.q, Succeeded=4*NTASKS, Pending=NTASKS)
        assert 0 == console.qdo(['rerun', self.qname, '--force'])
        _test_ntasks(self.q, Pending=5*NTASKS)

        #----- qdo pause
        assert self.q.state == 'Active'
        assert 0 == console.qdo(['pause', self.qname])
        assert self.q.state == 'Paused'
        assert self.q.get() is None

        #----- qdo resume
        assert 0 == console.qdo(['resume', self.qname])
        assert self.q.state == 'Active'
        assert self.q.get() is not None
        _test_ntasks(self.q, Pending=5*NTASKS-1, Running=1)

        #----- qdo recover
        assert 0 == console.qdo(['recover', self.qname])
        _test_ntasks(self.q, Pending=5*NTASKS)

        #----- qdo list
        assert 0 == console.qdo(['list'])

    def test_multiword_script(self):
        import tempfile
        import stat
        with tempfile.NamedTemporaryFile(delete=False) as temp1, tempfile.NamedTemporaryFile(delete=False) as temp2:

            scriptfn = temp1.name
            outfn = temp2.name
            os.chmod(scriptfn, stat.S_IWUSR | stat.S_IXUSR | stat.S_IRUSR)
            temp1.write('echo A $* > %s' % outfn)
            temp1.flush()
            temp1.close()
            
            console.qdo(['add', self.qname, "1"])
            console.qdo(['do', self.qname, '--timeout', '1',
                         '--script', scriptfn])

            txt = open(outfn, 'r').read()
            print('Read text: "%s"' % txt)
            self.assertEqual(txt, 'A 1\n')

            console.qdo(['add', self.qname, "1"])
            console.qdo(['do', self.qname, '--timeout', '1',
                         '--script', '%s B' % scriptfn])

            txt = open(outfn, 'r').read()
            print('Read text: "%s"' % txt)
            self.assertEqual(txt, 'A B 1\n')

