"""
Test python API to qdo
"""

from __future__ import print_function

import sys
import os
import time
import unittest

import qdo

NTASKS = 3


def _test_ntasks(q, **kwargs):
    """
    Check the number of tasks in each state.
    If a state isn't listed in kwargs, assume 0.
    """
    ntasks = q.tasks(summary=True)
    for state in ['Waiting', 'Pending', 'Running', 'Succeeded', 'Failed', 'Killed']:
        n = kwargs[state] if (state in kwargs) else 0            
        failmsg = "{}={} instead of {}".format(state, ntasks[state], n)
        assert ntasks[state] == n, failmsg
    

class QueueTestCase(unittest.TestCase):
    def setUp(self):  # also tests create
        self.qname = 'TestBlatFoo'
        self.q = qdo.create(self.qname)
        self.cmdfile = self.qname + '.txt'

    def tearDown(self):
        self.q.delete()
        if os.path.exists(self.cmdfile):
            os.remove(self.cmdfile)

    def test_delete(self):
        self.q.delete()

        # connecting to the deleted queue should error:
        self.assertRaises(Exception, qdo.connect, self.qname)

        # recreate queue so that delete in tearDown doesn't fail
        self.q = qdo.create(self.qname)

    def test_remaining_methods(self):
        """Test several qdo commands.

        These are lumped into a single method because they use the changing
        state of the queue from one command to the next.
        """

        print("-- q.add")
        for i in range(NTASKS):
            cmd = 'echo Hello {}'.format(i)
            self.q.add(cmd)
        _test_ntasks(self.q, Pending=NTASKS)

        print("-- q.add_multiple")
        cmds = ['oops {}'.format(i) for i in range(NTASKS)]
        self.q.add_multiple(cmds)
        _test_ntasks(self.q, Pending=2*NTASKS)

        print("-- q.loadfile")
        fx = open(self.cmdfile, 'w')
        for i in range(NTASKS):
            fx.write('echo Goodbye {}\n'.format(i))
        fx.close()
        self.q.loadfile(self.cmdfile)

        _test_ntasks(self.q, Pending=3*NTASKS)
        self.q.loadfile(self.cmdfile, priority=1)
        _test_ntasks(self.q, Pending=4*NTASKS)

        print("-- q.do")
        self.q.do(timeout=1, quiet=True)
        _test_ntasks(self.q, Succeeded=3*NTASKS, Failed=NTASKS)

        print("-- q.tasks")
        tasks = self.q.tasks()
        assert len(tasks) == 4*NTASKS
        assert type(tasks[0]) == qdo.Task

        print("-- q.tasks while filtering by state")
        tasks = self.q.tasks(state=qdo.Task.SUCCEEDED)
        assert len(tasks) == 3*NTASKS
        tasks = self.q.tasks(state=qdo.Task.FAILED)
        assert len(tasks) == NTASKS

        print("-- q.tasks : get single task")
        t = self.q.tasks(id=tasks[0].id)
        assert t.id == tasks[0].id

        print("-- q.tasks : try to get invalid task id")
        self.assertRaises(ValueError, self.q.tasks, id=-1)

        print("-- q.retry")
        _test_ntasks(self.q, Succeeded=3*NTASKS, Failed=NTASKS)
        self.q.retry()
        _test_ntasks(self.q, Succeeded=3*NTASKS, Pending=NTASKS)

        print("-- q.rerun : get single task")
        self.q.rerun()
        _test_ntasks(self.q, Pending=4*NTASKS)

        print("-- q.get")
        t = self.q.get()
        assert t.state == qdo.Task.RUNNING
        _test_ntasks(self.q, Pending=4*NTASKS-1, Running=1)

        print("-- q.pop")
        t = self.q.pop()
        assert t.state == qdo.Task.RUNNING
        _test_ntasks(self.q, Pending=4*NTASKS-2, Running=2)

        print("-- q.recover")
        self.q.recover()
        _test_ntasks(self.q, Pending=4*NTASKS)

        print("-- q.pause, q.resume, q.state")
        assert self.q.state == qdo.Queue.ACTIVE
        self.q.pause()
        assert self.q.state == qdo.Queue.PAUSED
        assert self.q.get() is None
        self.q.resume()
        assert self.q.state == qdo.Queue.ACTIVE

        print("-- t.run")
        t = self.q.get()
        _test_ntasks(self.q, Pending=4*NTASKS-1, Running=1)
        t.run()
        _test_ntasks(self.q, Pending=4*NTASKS-1, Succeeded=1)

        print("-- qdo.queues")
        qq = qdo.queues()
        assert isinstance(qq, dict)
        assert self.q.name in qq

        print("-- task state manipulation")
        t = self.q.get()
        t.set_state(qdo.Task.WAITING)
        assert self.q.tasks(t.id).state == qdo.Task.WAITING

        message = 'epic fail'
        t.set_state(qdo.Task.FAILED, err=1, message=message)
        tx = self.q.tasks(t.id)
        assert tx.state == qdo.Task.FAILED
        assert tx.err == 1
        assert tx.message == message

    def test_with_script(self):
        self.q.add_multiple(['hello {}'.format(i) for i in range(NTASKS)])
        _test_ntasks(self.q, Pending=NTASKS)
        self.q.do(quiet=True, script='echo', timeout=1)
        _test_ntasks(self.q, Succeeded=NTASKS)

    def test_with_function(self):
        def blat(x):
            print(x)
    
        self.q.rerun()
        self.q.do(func=blat, timeout=1)

    def test_printing(self):
        print(self.q)
        print(self.q.status())
        self.q.print_task_state()
        self.q.print_tasks()
        self.q.print_tasks(state=qdo.Task.PENDING)
        self.q.print_tasks(state=qdo.Task.PENDING, verbose=True)
        qdo.print_queues()

    def test_tasks_as_lists(self):
        self.q.add_multiple([ [i,i+1] for i in range(NTASKS)])
        _test_ntasks(self.q, Pending=NTASKS)
        self.q.do(quiet=True, script='echo counting {} {}', timeout=1)
        _test_ntasks(self.q, Succeeded=NTASKS)

    def tests_tasks_as_dicts(self):
        self.q.add_multiple([ dict(a=i, b=i+1) for i in range(NTASKS)])
        _test_ntasks(self.q, Pending=NTASKS)
        self.q.do(quiet=True, script='echo counting again {a} {b}', timeout=1)
        _test_ntasks(self.q, Succeeded=NTASKS)

    def test_create_task(self):
        t = qdo.Task('blatfoo', self.qname)  #- use queue name as a string
        assert isinstance(t, qdo.Task)

        # On py2, test that unicode name also works.
        if sys.version_info[0] == 2:
            t = qdo.Task('blatfoo', unicode(self.qname))
            assert isinstance(t, qdo.Task)

        t = qdo.Task('blatfoo', self.q)  #- Queue object instead of name
        assert isinstance(t, qdo.Task)


class ExitCodeTestCase(unittest.TestCase):
    """Tests filtering by exit code.

    These tests cannot be run on the postgres backend. They require log
    functions not yet implemented.
    """

    def setUp(self):  # also tests create
        self.qname = 'TestExitCode'
        self.q = qdo.create(self.qname)

    def tearDown(self):
        self.q.delete()

    def test_filtering(self):
        id_e1 = self.q.add('exit 1')
        id_e2 = self.q.add('exit 2')
        self.q.do(timeout=1, quiet=True)

        tasks = self.q.tasks(exitcode=1)
        assert len(tasks) == 1
        assert tasks[0].id == id_e1

        tasks = self.q.tasks(exitcode=2)
        assert len(tasks) == 1
        assert tasks[0].id == id_e2

        # This should raise ValueError when filtering by both id and exitcode.
        self.assertRaises(ValueError, self.q.tasks, id=id_e1, exitcode=1)

        # Confirm that rerunning several times doesn't result in
        # multiple entries.
        self.q.rerun()
        self.q.do(timeout=1, quiet=True)
        self.q.rerun()
        self.q.do(timeout=1, quiet=True)
        tasks = self.q.tasks()
        assert len(tasks) == 2
        tasks = self.q.tasks(state='Failed')
        assert len(tasks) == 2
        tasks = self.q.tasks(exitcode=1)
        assert len(tasks) == 1

        # If tasks are in Pending state, they shouldn't get picked up
        # by exit code.
        self.q.rerun()
        tasks = self.q.tasks()
        assert len(tasks) == 2
        tasks = self.q.tasks(exitcode=1)
        assert len(tasks) == 0

        # Retry failures while filtering by exit code.
        self.q.do(timeout=1, quiet=True)
        _test_ntasks(self.q, Failed=2)
        self.q.retry(exitcode=1)
        _test_ntasks(self.q, Pending=1, Failed=1)
        self.q.retry(exitcode=2)
        _test_ntasks(self.q, Pending=2, Failed=0)


class TaskFailureTestCase(unittest.TestCase):
    """Test things that should fail to create a new task"""
    def test_incorrect_queue_name(self):
        self.assertRaises(ValueError, qdo.Task, 'blatfoo', 'invalidqueuename')

    def test_missing_queue_name(self):
        self.assertRaises(TypeError, qdo.Task, 'blatfoo')


# Tests for priorities
class PrioritiesTestCase(unittest.TestCase):
    def setUp(self):
        self.q = qdo.create('PrioQueue')

    def tearDown(self):
        self.q.delete()

    def test_add_priority(self):
        """Test q.add(..., priority=...)"""

        ids = {}
        for i in range(20):
            if i == 5:
                ids[i] = self.q.add('sleep 2', priority=10)
            elif i == 10:
                ids[i] = self.q.add('sleep 2', priority=20)
            elif i == 15:
                ids[i] = self.q.add('sleep 2', priority=30)
            else:
                ids[i] = self.q.add('sleep 2')  #- default priority is 0

        self.q.do(runtime=1, quiet=True)
        t1=self.q.tasks(id=ids[15])
        t2=self.q.tasks(id=ids[10])
        t3=self.q.tasks(id=ids[5])
        t4=self.q.tasks(id=ids[0])

        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.PENDING
        assert t3.state==qdo.Task.PENDING
        assert t4.state==qdo.Task.PENDING

        self.q.do(runtime=1, quiet=True)
        t1=self.q.tasks(id=ids[15])
        t2=self.q.tasks(id=ids[10])
        t3=self.q.tasks(id=ids[5])
        t4=self.q.tasks(id=ids[0])

        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.SUCCEEDED
        assert t3.state==qdo.Task.PENDING
        assert t4.state==qdo.Task.PENDING

        self.q.do(runtime=1, quiet=True)
        t1=self.q.tasks(id=ids[15])
        t2=self.q.tasks(id=ids[10])
        t3=self.q.tasks(id=ids[5])
        t4=self.q.tasks(id=ids[0])

        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.SUCCEEDED
        assert t3.state==qdo.Task.SUCCEEDED
        assert t4.state==qdo.Task.PENDING

        self.q.do(runtime=1, quiet=True)
        t1=self.q.tasks(id=ids[15])
        t2=self.q.tasks(id=ids[10])
        t3=self.q.tasks(id=ids[5])
        t4=self.q.tasks(id=ids[0])

        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.SUCCEEDED
        assert t3.state==qdo.Task.SUCCEEDED
        assert t4.state==qdo.Task.SUCCEEDED

    def test_add_multiple_priority(self):
        """Test q.add_multiple(..., priority=...)"""

        ids = {}
        commands = []
        priorities = []
        for i in range(20):
            commands.append('sleep 2')
            if i == 5:
                priorities.append(10)
            elif i == 10:
                priorities.append(20)
            elif i == 15:
                priorities.append(30)
            else:
                priorities.append(0)
        ids = self.q.add_multiple(commands, priorities=priorities)   

        self.q.do(runtime=1, quiet=True)
        t1=self.q.tasks(id=ids[15])
        t2=self.q.tasks(id=ids[10])
        t3=self.q.tasks(id=ids[5])
        t4=self.q.tasks(id=ids[0])

        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.PENDING
        assert t3.state==qdo.Task.PENDING
        assert t4.state==qdo.Task.PENDING

        self.q.do(runtime=1, quiet=True)
        t1=self.q.tasks(id=ids[15])
        t2=self.q.tasks(id=ids[10])
        t3=self.q.tasks(id=ids[5])
        t4=self.q.tasks(id=ids[0])

        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.SUCCEEDED
        assert t3.state==qdo.Task.PENDING
        assert t4.state==qdo.Task.PENDING

        self.q.do(runtime=1, quiet=True)
        t1=self.q.tasks(id=ids[15])
        t2=self.q.tasks(id=ids[10])
        t3=self.q.tasks(id=ids[5])
        t4=self.q.tasks(id=ids[0])

        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.SUCCEEDED
        assert t3.state==qdo.Task.SUCCEEDED
        assert t4.state==qdo.Task.PENDING

        self.q.do(runtime=1, quiet=True)
        t1=self.q.tasks(id=ids[15])
        t2=self.q.tasks(id=ids[10])
        t3=self.q.tasks(id=ids[5])
        t4=self.q.tasks(id=ids[0])

        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.SUCCEEDED
        assert t3.state==qdo.Task.SUCCEEDED
        assert t4.state==qdo.Task.SUCCEEDED

    def test_add_dependencies(self):
        """Test q.add(..., requires=...)"""

        t_first_id=self.q.add('sleep 3', priority=2)

        q_intermediate=self.q.add("sleep 1", priority=1)
        q_second_id=self.q.add('sleep 2', requires=t_first_id, priority=4)
        t1=self.q.tasks(id=t_first_id)
        ti=self.q.tasks(id=q_intermediate)
        t2=self.q.tasks(id=q_second_id)
        assert t1.state==qdo.Task.PENDING
        assert t2.state==qdo.Task.WAITING
        assert ti.state==qdo.Task.PENDING

        self.q.do(runtime=1)
        t1=self.q.tasks(id=t_first_id)
        ti=self.q.tasks(id=q_intermediate)
        t2=self.q.tasks(id=q_second_id)
        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.PENDING
        assert ti.state==qdo.Task.PENDING

        self.q.do(runtime=1)
        t1=self.q.tasks(id=t_first_id)
        ti=self.q.tasks(id=q_intermediate)
        t2=self.q.tasks(id=q_second_id)
        assert t1.state==qdo.Task.SUCCEEDED

        # The depending task should have been executed since its parent task
        # is done and has more priority:
        assert t2.state==qdo.Task.SUCCEEDED

        assert ti.state==qdo.Task.PENDING
        print(t1.state, t2.state, ti.state)

    def test_add_multiple_dependencies(self):
        t_parent_id=self.q.add('sleep 3', priority=2)
        t_inter_id=self.q.add("sleep 1", priority=1)

        id_list=self.q.add_multiple(tasks=["sleep 2", "sleep 2"], 
                               priorities=[2,3],
                               requires=[t_parent_id, t_parent_id])
        t_c_2_id=id_list[0]
        t_c_3_id=id_list[1]

        list_ids=[t_parent_id, t_inter_id, t_c_2_id, t_c_3_id]
        (t1, t2, t3, t4)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.PENDING
        assert t2.state==qdo.Task.PENDING
        assert t3.state==qdo.Task.WAITING
        assert t4.state==qdo.Task.WAITING
        self.q.do(runtime=1)
        # job with higher prio but no dep runs: t1
        (t1, t2, t3, t4)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.PENDING
        assert t3.state==qdo.Task.PENDING
        assert t4.state==qdo.Task.PENDING
        self.q.do(runtime=1)
        # job with higher prio but and dep fulfilled runs: t4
        (t1, t2, t3, t4)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.PENDING
        assert t3.state==qdo.Task.PENDING
        assert t4.state==qdo.Task.SUCCEEDED
        self.q.do(runtime=1)
        # job with higher prio but and dep fulfilled runs: t3
        (t1, t2, t3, t4)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.PENDING
        assert t3.state==qdo.Task.SUCCEEDED
        assert t4.state==qdo.Task.SUCCEEDED
        self.q.do(runtime=1)
        (t1, t2, t3, t4)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.SUCCEEDED
        assert t3.state==qdo.Task.SUCCEEDED
        assert t4.state==qdo.Task.SUCCEEDED

    def test_add_multiple_dependencies_2(self):
        """Test q.add_multiple(...) where one job depends on multiple)"""

        t_np=self.q.add('sleep 2', priority=2)
        t_p1=self.q.add('sleep 3', priority=2)
        t_p2=self.q.add('sleep 3', priority=2)
        t_c=self.q.add('sleep 1', priority=3, requires=[t_p1, t_p2])
        list_ids=[t_np, t_p1, t_p2, t_c]
        (t1, t2, t3, t4)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.PENDING
        assert t2.state==qdo.Task.PENDING
        assert t3.state==qdo.Task.PENDING
        assert t4.state==qdo.Task.WAITING
        self.q.do(runtime=1)
        (t1, t2, t3, t4)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.PENDING
        assert t3.state==qdo.Task.PENDING
        assert t4.state==qdo.Task.WAITING
        self.q.do(runtime=1)
        (t1, t2, t3, t4)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.SUCCEEDED
        assert t3.state==qdo.Task.PENDING
        assert t4.state==qdo.Task.WAITING
        self.q.do(runtime=1)
        (t1, t2, t3, t4)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.SUCCEEDED
        assert t3.state==qdo.Task.SUCCEEDED
        assert t4.state==qdo.Task.PENDING
        self.q.do(runtime=1)
        (t1, t2, t3, t4)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.SUCCEEDED
        assert t2.state==qdo.Task.SUCCEEDED
        assert t3.state==qdo.Task.SUCCEEDED
        assert t4.state==qdo.Task.SUCCEEDED

    def test_parent_task_fails(self):
        """Test what happens when parent task fails"""

        t_np=self.q.add('sleep 2', priority=2)
        t_p1=self.q.add('sleep 2; kkkk', priority=3)
        t_c=self.q.add('sleep 1', priority=4, requires=[t_p1])
        list_ids=[t_np, t_p1, t_c]
        (t1, t2, t3)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.PENDING
        assert t2.state==qdo.Task.PENDING
        assert t3.state==qdo.Task.WAITING
        self.q.do(runtime=1)
        (t1, t2, t3)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.PENDING
        assert t2.state==qdo.Task.FAILED
        assert t3.state==qdo.Task.PENDING

        # check that retry puts child task in waiting
        self.q.retry()
        (t1, t2, t3)=[self.q.tasks(id=t) for t in list_ids]
        assert t1.state==qdo.Task.PENDING
        assert t2.state==qdo.Task.PENDING
        assert t3.state==qdo.Task.WAITING
